//Project types
export const FORM_PROJECT = 'FORM_PROJECT'
export const GET_PROJECTS = 'GET_PROJECTS'
export const ADD_PROJECT = 'ADD_PROJECT'
export const VALIDATE_FORM = 'VALIDATE_FORM'
export const ACTIVE_PROJECT = 'ACTIVE_PROJECT'
export const DELETE_PROJECT = 'DELETE_PROJECT'

//Task types
export const TASK_PROJECT = 'TASK_PROJECT'
export const ADD_TASK = 'ADD_TASK'
export const VALIDATE_TASK = 'VALIDATE_TASK'
export const DELETE_TASK = 'DELETE_TASK'
export const STATE_TASK = 'STATE_TASK'
export const CURRENT_TASK = 'CURRENT_TASK'
export const EDIT_TASK = 'EDIT_TASK'

//alert types
export const SHOW_ALERT = 'SHOW_ALERT'
export const HIDE_ALERT = 'HIDE_ALERT'

//auth types
export const REGISTRY_SUCCEDED = 'REGISTRY_SUCCEDED'
export const REGISTRY_ERROR = 'REGISTRY_ERROR'
export const GET_USER = 'GET_USER'
export const LOGIN_SUCCEDED = 'LOGIN_SUCCEDED'
export const LOGIN_ERROR = 'LOGIN_ERROR'
export const LOG_OUT = 'LOG_OUT'