import { useReducer } from 'react'
import TaskContext from './taskContext'
import taskReducer from './taskReducer'
import { 
    TASK_PROJECT,
    ADD_TASK,
    VALIDATE_TASK,
    DELETE_TASK,
    STATE_TASK,
    CURRENT_TASK,
    EDIT_TASK
} from '../../types'
import { v4 as uuidv4 } from 'uuid'

const TareaState = props =>{
    const projectTasks = [
        { name: 'Elegir Plataforma', state: true, projectId: 1 },
        { name: 'Elegir Colores', state: false, projectId: 2 },
        { name: 'Elegir Plataformas de Pago', state: false, projectId: 3 },
        { name: 'Elegir Hosting', state: true, projectId: 4 },
        { name: 'Elegir Plataforma', state: true, projectId: 1 },
        { name: 'Elegir Colores', state: false, projectId: 2 },
        { name: 'Elegir Plataformas de Pago', state: false, projectId: 3 },
        { name: 'Elegir Hosting', state: true, projectId: 4 },
        { name: 'Elegir Plataforma', state: true, projectId: 1 },
        { name: 'Elegir Colores', state: false, projectId: 1 },
        { name: 'Elegir Plataformas de Pago', state: false, projectId: 3 },
        { name: 'Elegir Hosting', state: true, projectId: 3 },
    ].map((task, index) => ({ id: index+1, ...task }))
    const initialState = {
        tasks: projectTasks,
        projectTasks: null,
        errorTask: false,
        selectedTask: null
    }

    // creating dispath and state

    const [state, dispatch] = useReducer(taskReducer, initialState)

    //Creating functions

    //get task of one project
    const getTasks = projectId => {
        dispatch({
            type: TASK_PROJECT,
            payload: projectId
        })
    }

    // adding task to a project
    const addTask = task =>{
        task.id = uuidv4()
        dispatch({
            type: ADD_TASK,
            payload: task
        })
    }
    const validateTask = () =>{
        dispatch({
            type: VALIDATE_TASK
        })
    }
    const deleteTask = id =>{
        dispatch({
            type: DELETE_TASK,
            payload: id
        })
    }
    const changeStateTask = task =>{
        dispatch({
            type: STATE_TASK,
            payload: task
        })
    }
    const saveCurrentTask = task =>{
        dispatch({
            type: CURRENT_TASK,
            payload: task
        })
    }
    const editCurrentTask = task =>{
        dispatch({
            type: EDIT_TASK,
            payload: task
        })
    }

    return (
        <TaskContext.Provider
            value={{
                tasks: state.tasks,
                projectTasks: state.projectTasks,
                errorTask: state.errorTask,
                selectedTask: state.selectedTask,
                getTasks,
                addTask,
                validateTask,
                deleteTask,
                changeStateTask,
                saveCurrentTask,
                editCurrentTask,
            }}
        >
            {props.children}
        </TaskContext.Provider>
    )
}

export default TareaState