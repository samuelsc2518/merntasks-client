import { useReducer } from 'react'
import authReducer from './authReducer'
import authContext from './authContext'
import { REGISTRY_SUCCEDED, REGISTRY_ERROR, GET_USER, LOGIN_SUCCEDED, LOGIN_ERROR, LOG_OUT } from '../../types'
import clientAxios from '../../config/axios'
import tokenAuth from '../../config/tokenAuth'

const AuthState = props =>{

    const initialState = {
        token: localStorage.getItem('token'),
        authenticated: null,
        user: null,
        msg: null
    }

    const [state, dispatch] = useReducer(authReducer, initialState)

    const registerUser = async (data) =>{
        try {
            const response = await clientAxios.post('/api/users', data)

            dispatch({
                type: REGISTRY_SUCCEDED,
                payload: response.data
            })

            //get the user
            authenticatedUser()
        } catch (e) {
            // console.log(e.response)
            const msg = {
                content: e.response.data.msg,
                category: 'alerta-error'
            }

            dispatch({
                type: REGISTRY_ERROR,
                payload: msg
            })
        }
    }

    //return the authenticated user
    const authenticatedUser = async () =>{
        const token = localStorage.getItem('token')

        if(token) tokenAuth(token)

        try {
            const response = await clientAxios.get('/api/auth')
            dispatch({
                type: GET_USER,
                payload: response.data
            })
        } catch (e) {
            console.log(e.response)
            dispatch({
                type: LOGIN_ERROR
            })
        }
    }

    //login user
    const loginUser = async data =>{
        try {
            const response = await clientAxios.post('/api/auth', data)
            
            dispatch({
                type: LOGIN_SUCCEDED,
                payload: response.data
            })
            
            //get the user
            authenticatedUser()
        } catch (e) {
            console.log(e.response)
            let msg = {}
            if(e.response.data.msg){
                msg = {
                    content: 'The email or password are incorrect',
                    category: 'alerta-error'
                }
            }else{
                if(e.response.data.errors.length > 1){
                    msg = {
                        content: 'The email or password are incorrect',
                        category: 'alerta-error'
                    }
                }else{
                    msg = {
                        content: e.response.data.errors[0].msg,
                        category: 'alerta-error'
                    }
                }
            }           
            

            dispatch({
                type: LOGIN_ERROR,
                payload: msg
            })


        }
    }

    //log out

    const logOut = () =>{
        dispatch({
            type: LOG_OUT
        })
    }

    

    return (
        <authContext.Provider
            value={{
                token: state.token,
                authenticated: state.authenticated,
                user: state.user,
                msg: state.msg,
                registerUser,
                loginUser,
                authenticatedUser,
                logOut
            }}
        >
            {props.children}
        </authContext.Provider>
    )

}

export default AuthState