import { REGISTRY_SUCCEDED, REGISTRY_ERROR, GET_USER, LOGIN_SUCCEDED, LOGIN_ERROR, LOG_OUT } from '../../types'

const alertState = (state, action) =>{
    switch(action.type){
        
        case LOGIN_SUCCEDED:
        case REGISTRY_SUCCEDED:
            localStorage.setItem('token', action.payload.token)

            return {
                ...state,
                authenticated: true,
                msg: null,
            }
        case LOG_OUT:
        case LOGIN_ERROR:   
        case REGISTRY_ERROR:
            localStorage.removeItem('token')

            return {
                ...state,
                authenticated: null,
                msg: action.payload,
                token: null,
                user: null
            }
        case GET_USER:
            return {
                ...state,
                user: action.payload.user,
                authenticated: true
            }
        default:
            return state
    }
}

export default alertState