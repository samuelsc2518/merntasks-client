import projectContext from "./projectContext";
import projectReducer from "./projectReducer";
import { 
    FORM_PROJECT, 
    GET_PROJECTS,
    ADD_PROJECT,
    VALIDATE_FORM,
    ACTIVE_PROJECT,
    DELETE_PROJECT
} from "../../types";
import { useReducer } from "react";
import { v4 as uuidv4 } from "uuid"

const ProjectState = props => {
    
    const projects = [
        { id: 1, name: 'Tienda Virtual' },
        { id: 2, name: 'Intranet' },
        { id: 3, name: 'Diseño de sitio web' },
        { id: 4, name: 'MERN' },
    ]

    const initialState = {
        projects: [],
        form: false,
        errorform: false,
        project: null

    }

    //Dispatch

    const [state, dispatch] = useReducer(projectReducer, initialState)

    // functions for CRUD

    const showForm = () => {
        dispatch({
            type: FORM_PROJECT
        })
    }

    //get projects
    const getProjects = () =>{
        dispatch({
            type: GET_PROJECTS,
            payload: projects
        })
    }

    //add new project
    const addProject = project => {
        project.id = uuidv4()

        //add project to state
        dispatch({
            type: ADD_PROJECT,
            payload: project
        })
    }

    //validating from
    const showError = () =>{
        dispatch({
            type: VALIDATE_FORM,

        })
    }

    //selecting project
    const selectProject = projectId =>{
        dispatch({
            type: ACTIVE_PROJECT,
            payload: projectId
        })
    }

    //deleting one project
    const deleteProject = projectId =>{
        dispatch({
            type: DELETE_PROJECT,
            payload: projectId
        })
    }

    return (
        <projectContext.Provider
            value={{
                projects: state.projects,
                form: state.form,
                errorform: state.errorform,
                project: state.project,
                showForm,
                getProjects,
                addProject,
                showError,
                selectProject,
                deleteProject,
                
            }}
        >
            {props.children}
        </projectContext.Provider>
    )
}

export default ProjectState