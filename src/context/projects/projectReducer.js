import { 
    GET_PROJECTS, 
    FORM_PROJECT, 
    ADD_PROJECT, 
    VALIDATE_FORM, 
    ACTIVE_PROJECT,
    DELETE_PROJECT
} from '../../types'

const reducer = (state, action) => {
    switch(action.type){
        case FORM_PROJECT:
            return {
                ...state,
                form: true
            }
        case GET_PROJECTS:
            return {
                ...state,
                projects: action.payload
            }
        case ADD_PROJECT:
            return {
                ...state,
                projects: [
                    ...state.projects,
                    action.payload
                ],
                form: false,
                errorform: false
            }
        case VALIDATE_FORM:
            return {
                ...state,
                errorform: true
            }
        case ACTIVE_PROJECT:
            return {
                ...state,
                project: state.projects.filter(project => project.id === action.payload)
            }
        case DELETE_PROJECT:
            return {
                ...state,
                projects: state.projects.filter(project => project.id !== action.payload),
                project: null
            }
        default:
            return state
    }
}

export default reducer