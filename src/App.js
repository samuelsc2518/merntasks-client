import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import ProjectState from './context/projects/ProjectState';
import TareaState from './context/tasks/taskState';
import AlertState from './context/alerts/AlertState';
import AuthState from './context/auth/AuthState';
import Login from './components/auth/Login';
import NewAccount from './components/auth/NewAccount'
import Projects from './components/projects/Projects'
import tokenAuth from './config/tokenAuth'

//check if exist a token
const token = localStorage.getItem('token')
if(token){
  tokenAuth(token)
}

export default function App() {
  return (
    
    <AuthState>
      <AlertState>
        <TareaState>
          <ProjectState>
            <Router>
              <Routes>
                <Route path='/' element={<Login />} />
                <Route path='/new-account' element={<NewAccount />} />
                <Route path='/projects' element={<Projects />} />
              </Routes>
            </Router>
          </ProjectState>
        </TareaState>
      </AlertState>
    </AuthState>

  );
}
