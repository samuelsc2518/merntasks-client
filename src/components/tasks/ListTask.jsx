import Task from './Task'
import projectContext from '../../context/projects/projectContext'
import TaskContext from '../../context/tasks/taskContext'
import { useContext } from 'react'
import { CSSTransition, TransitionGroup } from 'react-transition-group'

export default function ListTask() {

    const projectsContext = useContext(projectContext)
    const { project, deleteProject } = projectsContext

    const tasksContext = useContext(TaskContext)
    const { projectTasks } = tasksContext

    if(!project) return <h2>Select one project</h2>

    const [ activeProject ] = project

    // const projectTasks = []

    return (
        <>
            <h2>Project: {activeProject.name}</h2>

            <ul className="listado-tareas">
                {projectTasks.length === 0
                ? (<li className="tarea"><p>No hay tareas</p></li>)
                : <TransitionGroup>
                    {projectTasks.map((task, index) =>(
                    <CSSTransition 
                        key={task.id}
                        timeout={200}
                        classNames="tarea"
                    >
                        <Task task={task} />
                    </CSSTransition>
                    ))}
                </TransitionGroup>}
            </ul>

            <button
                type='button'
                className='btn btn-eliminar'
                onClick={() => deleteProject(activeProject.id)}
            >Delete &times;</button>
        </>
    )
}
