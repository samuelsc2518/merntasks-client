import projectContext from "../../context/projects/projectContext"
import TaskContext from "../../context/tasks/taskContext"
import { useContext, useState, useEffect } from "react"

export default function FormTask() {

    const projectsContext = useContext(projectContext)
    const { project } = projectsContext

    const tasksContext = useContext(TaskContext)
    const { addTask, validateTask, errorTask, getTasks, selectedTask, editCurrentTask } = tasksContext

    const [task, setTask] = useState({
        name: ''
    })
    const { name } = task
    const handleChange = e =>{
        setTask({
            ...task,
            [e.target.name]: e.target.value
        })
    }

    useEffect(() => {
        if(selectedTask !== null){
            setTask(selectedTask)
        }else{
            setTask({
                name: ''
            })
        }
    }, [selectedTask])

    if(!project) return null

    const [ activeProject ] = project

    const onSubmit = e =>{
        e.preventDefault()

        //validate
        if(task.name.trim() === ''){
            validateTask()
            return
        }

        if(selectedTask !== null){
            //editing the task
            editCurrentTask(task)
        }else{
            //add the new task to state
            task.projectId = activeProject.id
            task.state = false
            addTask(task)
        }
        
        //get new tasks
        getTasks(activeProject.id)

        //reset
        setTask({
            name: ''
        })
    }

    return (
        <div className="formulario">
            <form
                onSubmit={onSubmit}
            >
                <div className="contenedor-input">
                    <input 
                        type="text"
                        className="input-text"
                        placeholder="Name of the task..."
                        name="name"
                        onChange={handleChange}
                        value={name}
                    />
                </div>

                <div className="contenedor-input">
                    <input 
                        type="submit"
                        className="btn btn-primario btn-submit btn-block"
                        value={selectedTask ? "Edit Task" : "Add task"}
                    />
                </div>
            </form>
            {errorTask
            ? <p className="mensaje error">The name of the task is required</p>
            : null}
        </div>
    )
}
