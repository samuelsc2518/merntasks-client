import { useContext } from "react"
import taskContext from "../../context/tasks/taskContext"
import projectContext from "../../context/projects/projectContext"

export default function Task({ task }) {

    const tasksContext = useContext(taskContext)
    const { deleteTask, getTasks, changeStateTask, saveCurrentTask } = tasksContext

    
    const projectsContext = useContext(projectContext)
    const { project } = projectsContext
    const [ actualProject ] = project

    //deleting task
    const deletingTask = id =>{
        deleteTask(id)
        getTasks(actualProject.id)
    }

    const changeState = task => {
        task.state = !task.state
        changeStateTask(task)
    }

    const saveTheCurrentTask = task =>{
        saveCurrentTask(task)
    }

    return (
        <li className="tarea sombra">
            <p>{task.name}</p>

            <div className="estado">
                {task.state
                ? (
                    <button
                        type="button"
                        className="completo"
                        onClick={() => changeState(task)}
                    >Completed</button>
                )
                : (
                    <button
                        type="button"
                        className="incompleto"
                        onClick={() => changeState(task)}
                    >Incompleted</button>
                )}
            </div>

            <div className="acciones">
                <button
                    type="button"
                    className="btn btn-primario"
                    onClick={() => saveTheCurrentTask(task)}
                >Edit</button>
                
                <button
                    type="button"
                    className="btn btn-secundario"
                    onClick={() => deletingTask(task.id)}
                >Delete</button>
            </div>
        </li>
    )
}
