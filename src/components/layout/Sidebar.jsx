import NewProject from "../projects/NewProject"
import List from "../projects/List"

export default function Sidebar() {
    return (
        <aside>
            <h1>MERN <span>Tasks</span></h1>

            <NewProject />

            <div className="proyectos">
                <h2>Tus Proyectos</h2>
                <List />
            </div>
        </aside>
    )
}
