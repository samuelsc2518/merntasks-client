import { useContext, useEffect } from "react"
import authContext from "../../context/auth/authContext"

export default function Bar() {

    const authsContext = useContext(authContext)
    const { authenticatedUser, user, logOut } = authsContext

    useEffect(() => {
        authenticatedUser()
    }, [])

    return (
        <header className="app-header">
            {user ? <p className="nombre-usuario">Hello <span>{user.name}</span></p> : null}
            

            <nav className="nav-principal">
                <button
                    className="btn btn-blank cerrar-sesion"
                    onClick={() => logOut()}
                >Log out</button>
            </nav>
        </header>
    )
}
