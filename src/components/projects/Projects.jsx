import { useContext, useEffect } from "react"
import Sidebar from "../layout/Sidebar"
import Bar from "../layout/Bar"
import FormTask from "../tasks/FormTask"
import ListTask from "../tasks/ListTask"
import authContext from "../../context/auth/authContext"

export default function Projects() {

    const authsContext = useContext(authContext)
    const { authenticatedUser } = authsContext

    useEffect(() =>{
        authenticatedUser()
    }, [])

    return (
        <div className="contenedor-app">
            
            <Sidebar />

            <div className="seccion-principal">
                <Bar />

                <main>
                    <FormTask />

                    <div className="contenedor-tareas">
                        <ListTask />
                    </div>
                </main>
            </div>
        </div>
    )
}
