import projectContext from "../../context/projects/projectContext"
import taskContext from "../../context/tasks/taskContext"
import { useContext } from "react"

export default function Project({ project }) {

    const projectsContext = useContext(projectContext)
    const tasksContext = useContext(taskContext)

    const { selectProject } = projectsContext
    const { getTasks } = tasksContext

    const selectingProject = id =>{
        selectProject(id) //selecting the project

        getTasks(id)
    }

    return (
        <li>
            <button
                type="button"
                className="btn btn-blank"
                onClick={() => selectingProject(project.id)}
            >{project.name}</button>
        </li>
    )
}
