import { useState, useContext } from "react"
import projectContext from "../../context/projects/projectContext"

export default function NewProject() {

    //get state
    const projectsContext = useContext(projectContext)
    const { form, showForm, addProject, errorform, showError } = projectsContext

    // State of the project
    const [project, setProject] = useState({
        name: ''
    })

    const { name } = project

    const onChangeProject = e =>{
        setProject({
            ...project,
            [e.target.name]: e.target.value
        })
    }

    const onSubmitProject = e =>{
        e.preventDefault()


        //validation
        if(name === '') {
            showError()
            return
        }


        //add to state
        addProject(project)

        //reset form
        setProject({
            name: ''
        })
    }

    return (
        <>
            <button
                type="button"
                className="btn btn-block btn-primario"
                onClick={() => showForm()}
            >New Project</button>

            {form
            ? <form 
                onSubmit={onSubmitProject}
                className="formulario-nuevo-proyecto"
            >
                <input 
                    type="text" 
                    name="name"
                    className="input-text"
                    placeholder="Name of Project"
                    onChange={onChangeProject}
                    value={name}
                />

                <input 
                    type="submit" 
                    className="btn btn-primario btn-block"
                    value="Add Project"
                />
            </form>
            : null}

            {errorform
            ?   <p className="mensaje error">The name of project is required</p>
            : null}
        </>
    )
}
