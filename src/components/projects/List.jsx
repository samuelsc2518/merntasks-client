import Project from "./Project"
import projectContext from "../../context/projects/projectContext"
import { useContext, useEffect } from "react"
import { TransitionGroup, CSSTransition, Transition } from "react-transition-group"

export default function List() {

    //get projects of state
    const projectsContext = useContext(projectContext)
    const { projects, getProjects } = projectsContext

    useEffect(() => {
        getProjects()
        // eslint-disable-next-line
    }, [])


    //if it have projects
    if(projects.length === 0) return <p>Create one new project</p>

    return (
        <ul className="listado-proyectos">
            <TransitionGroup>
                {projects.map(project =>(
                    <CSSTransition
                        key={project.id}
                        timeout={200}
                        classNames="proyecto"
                    >
                        <Project project={project} />
                    </CSSTransition>
                ))}
            </TransitionGroup>
        </ul>
    )
}
