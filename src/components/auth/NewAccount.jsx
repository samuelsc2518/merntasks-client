import { useState, useContext, useEffect } from "react"
import { Link } from "react-router-dom"
import alertContext from "../../context/alerts/alertContext"
import authContext from "../../context/auth/authContext"
import { useNavigate } from 'react-router-dom'

export default function NewAccount(props) {

    const navigate = useNavigate()

    const alertsContext = useContext(alertContext)
    const { alert, showAlert } = alertsContext
    
    const authsContext = useContext(authContext)
    const { registerUser, msg, authenticated } = authsContext

    const [user, setUser] = useState({
        name: '',
        email: '',
        password: '',
        confirm: ''
    })

    const { email, password, name, confirm } = user

    //when authenticated or duplicated registry
    useEffect(() => {

        if(authenticated){
            navigate('/projects', { replace: true })
        }

        if(msg){
            showAlert(msg.content, msg.category)
        }

    }, [msg, authenticated])

    const onChange = e => {
        setUser({
            ...user,
            [e.target.name]: e.target.value
        })
    }

    const onSubmit = e => {
        e.preventDefault()

        // validation
        if(name.trim() === '' || email.trim() === '' || password.trim() === '' || confirm.trim() === ''){
            showAlert('All fields required', 'alerta-error')
            return
        }
        // password min 6 char
        if(password.length < 6){
            showAlert('The password needs 6 minimum characters', 'alerta-error')
            return
        }

        // 2 password same
        if(password !== confirm){
            showAlert('The passwords is not the same', 'alerta-error')
            return
        }

        // pass to action
        registerUser({
            name,
            email,
            password
        })
    }

    return (
                    
            <div className="form-usuario">
                {alert
                ? <div className={`alerta ${alert.category}`}>{alert.msg}</div>
                : null}
                <div className="contenedor-form sombra-dark">
                    <h1>Create new account</h1>
    
                    <form onSubmit={onSubmit}>
                        <div className="campo-form">
                            <label htmlFor="name">Name</label>
                            <input 
                                type="text" 
                                name="name"  
                                id="name"
                                placeholder="Your Name"
                                onChange={onChange}
                                value={name}
                            />
                        </div>
                        <div className="campo-form">
                            <label htmlFor="email">Email</label>
                            <input 
                                type="email" 
                                name="email" 
                                id="email"
                                placeholder="Your Email"
                                onChange={onChange}
                                value={email}
                            />
                        </div>
                        <div className="campo-form">
                            <label htmlFor="password">Password</label>
                            <input 
                                type="password" 
                                name="password" 
                                id="password"
                                placeholder="Your Password"
                                onChange={onChange}
                                value={password}
                            />
                        </div>
                        <div className="campo-form">
                            <label htmlFor="confirm">Confirm Password</label>
                            <input 
                                type="password" 
                                name="confirm" 
                                id="confirm"
                                placeholder="Repeat Your Password"
                                onChange={onChange}
                                value={confirm}
                            />
                        </div>
                        <div className="campo-form">
                            <input 
                                type="submit" 
                                value="Sign up" 
                                className="btn btn-primaro btn-block" />
                        </div>
                    </form>
    
                    <Link to={'/'} className="enlace-cuenta">
                        Do you have an account?
                    </Link>
                </div>
            </div>
    )
}
