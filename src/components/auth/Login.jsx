import { useState, useContext, useEffect } from "react"
import { Link } from 'react-router-dom'
import alertContext from "../../context/alerts/alertContext"
import authContext from "../../context/auth/authContext"
import { useNavigate } from 'react-router-dom'

export default function Login() {
    
    const navigate = useNavigate()

    const alertsContext = useContext(alertContext)
    const { alert, showAlert } = alertsContext
    
    const authsContext = useContext(authContext)
    const { loginUser, msg, authenticated } = authsContext

    const [user, setUser] = useState({
        email: '',
        password: ''
    })

    const { email, password } = user

    useEffect(() => {

        if(authenticated){
            navigate('/projects', { replace: true })
        }

        if(msg){
            showAlert(msg.content, msg.category)
        }

    }, [msg, authenticated])

    const onChange = e => {
        setUser({
            ...user,
            [e.target.name]: e.target.value
        })
    }

    const onSubmit = async e => {
        e.preventDefault()

        // validation
        if(email.trim() === '' || password.trim() === ''){
            showAlert('All fields required', 'alerta-error')
            return
        }

        // pass to action
        await loginUser(user)


    }

    return (
        
        <div className="form-usuario">
            {alert
            ? <div className={`alerta ${alert.category}`}>{alert.msg}</div>
            : null}
            <div className="contenedor-form sombra-dark">
                <h1>Login</h1>

                <form onSubmit={onSubmit}>
                    <div className="campo-form">
                        <label htmlFor="email">Email</label>
                        <input 
                            type="email" 
                            name="email" 
                            id="email"
                            placeholder="Your Email"
                            onChange={onChange}
                            value={email}
                        />
                    </div>
                    <div className="campo-form">
                        <label htmlFor="password">Password</label>
                        <input 
                            type="password" 
                            name="password" 
                            id="password"
                            placeholder="Your Password"
                            onChange={onChange}
                            value={password}
                        />
                    </div>
                    <div className="campo-form">
                        <input 
                            type="submit" 
                            value="Login in" 
                            className="btn btn-primaro btn-block" />
                    </div>
                </form>

                <Link to={'/new-account'} className="enlace-cuenta">
                    Create New Account
                </Link>
            </div>
        </div>
        
    )
}
